const data = {
  products: [
    {
      id: '1',
      name: 'Pongal',
      price: 40,
      image: 'https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/umgujtwv7ggl8uplxn3f',
    },
    {
      id: '2',
      name: 'Meals',
      price: 80,
      image: 'https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/vqntkxnetpgfivsyrpij',
    }, 
    {
      id: '3',
      name: 'Poori',
      price: 40,
      image: 'https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/ap2ejccdwlcemofxzqg6',
    },
    {
      id: '4',
      name: 'Dosa',
      price: 50,
      image: 'https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_508,h_320,c_fill/dtcwa1rxdskp3crqvpr3',
    },
    {
      id: '5',
      name: 'Biryani',
      price: 99,
      image: 'https://res.cloudinary.com/swiggy/image/upload/fl_lossy,f_auto,q_auto,w_1024/ourhpdqqx40mqdzcuwva',
    },
  ],
};
export default data;
