import Header from './components/Header';
import Main from './components/Main';
import Basket from './components/Basket';
import data from './data';
import ControlledCarousel from './components/Courosel';

import { useState , useEffect} from 'react';


const cartFromLocalStoage = JSON.parse(localStorage.getItem('cartItems') || '[]')
function App() {
  const { products } = data;
  const [cartItems, setCartItems] = useState(cartFromLocalStoage);
  useEffect(() => {
    localStorage.setItem("cartItems",JSON.stringify(cartItems));
  }, [cartItems]);
  const onAdd = (product) => {
    const exist = cartItems.find((x) => x.id === product.id);
    if (exist) {
      setCartItems(
        cartItems.map((x) =>
          x.id === product.id ? { ...exist, qty: exist.qty + 1 } : x
        )
      );
    } else {
      setCartItems([...cartItems, { ...product, qty: 1 }]);
    }
  };
  const onRemove = (product) => {
    const exist = cartItems.find((x) => x.id === product.id);
    if (exist.qty === 1) {
      setCartItems(cartItems.filter((x) => x.id !== product.id));
    } else {
      setCartItems(
        cartItems.map((x) =>
          x.id === product.id ? { ...exist, qty: exist.qty - 1 } : x
        )
      );
    }
  };
 
  return (
    <div className="App">

      <Header countCartItems={cartItems.length}></Header>
      <div className="row">
      <ControlledCarousel></ControlledCarousel>

      </div>
      <div className="row">
        <Main products={products} onAdd={onAdd}></Main>
        </div>

        <div className="row1">
        <Basket
          cartItems={cartItems}
          onAdd={onAdd}
          onRemove={onRemove}
        ></Basket>
        
        
      </div>
    </div>
  );
}

export default App;


