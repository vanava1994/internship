import React from 'react';

export default function Header(props) {
  return (
    <header className="block row center">
      <div>
        <a href="#/">
        <img className="logo" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTTnl7vT5xZqYdorAjiclFSVcfOqVYQnfXSkA&usqp=CAU" alt="Avatar" />
        </a>
      </div>
      <div>
        <a  href="#/cart">
          Cart{' '}
          {props.countCartItems ? (
            <button className="badge">{props.countCartItems}</button>
          ) : (
            ''
          )}
        </a>{' '}

      </div>
    </header>
  );
}
