import React from 'react';

export default function Product(props) {
  const { product, onAdd } = props;
  console.log(product);
  return (
    <div className="card">
      <img className="small" src={product.image} alt={product.name} />
      <h3>{product.name}</h3>
      <div>Price : ₹  {product.price}</div>
      <div>
        <button onClick={() => onAdd(product)}>Add To Cart</button>
      </div>
    </div>
  );
}
