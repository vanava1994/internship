import React, {useState} from 'react';
import ListItem from './todo/listitem';
  
export default function App() {
const [todoName,setTodoName] = useState("");
const [list,setList] = useState([]);

const updateTodoName = (e) => {
  setTodoName(e.target.value)
}
const onDone =(item) =>{
console.log(item)
let newList = list.map(ListItem => {
  if (ListItem.id === item.id){
    return{...ListItem,completed:!ListItem.completed};
  }
  return ListItem
});

setList(newList)
}

const onDelete = item => {
  console.log(item)
  let newList = list.filter(ListItem => {
    if (ListItem.id === item.id){
      return false;
    }
    return true;
  });
  
  setList(newList)
  }

const addTodo = () =>{
  setList([...list,{
    id:Math.random(),
    name:todoName,
    completed:false
  }]);
  setTodoName("");
  console.log(todoName)
}

  return(
    <div>
      <h1>Todo List</h1>
      <input
      type ="text"
      placeholder="Enter list Items"
      value={todoName}
      onChange ={updateTodoName}
      />
      <button onClick={addTodo}>Add</button>
{
  list.map(item=><ListItem obj={item} onDone={onDone} onDelete={onDelete}/>)
}
    </div>
  )
}