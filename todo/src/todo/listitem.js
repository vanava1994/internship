
export default function ListItem(props){
    return(
        <div className={props.obj.completed && "completed"}>
            <p><span>{props.obj.name}  
            <button  onClick={()=> props.onDone(props.obj)}>Mark as Read</button>
            <button onClick={()=> props.onDelete(props.obj)}>Delete</button></span>
            </p>
        </div>
        );
}